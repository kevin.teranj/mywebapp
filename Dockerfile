FROM image-registry.openshift-image-registry.svc:5000/default/jws57-openjdk17-openshift-rhel8:5.7.8-6

RUN mkdir $JWS_HOME/configuracion

COPY sample.war $JWS_HOME/webapps/.

COPY conf_files/* $JWS_HOME/configuracion/.
